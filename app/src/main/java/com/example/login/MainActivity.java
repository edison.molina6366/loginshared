package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText email ;
    EditText pass ;

    SharedPreferences sharedPreferences;


    Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //MAPEO DE ELEMENTOS
        email=findViewById(R.id.email);
        pass=findViewById(R.id.password1);

        //definir Objeto SharedPreferenced
        sharedPreferences = getSharedPreferences("datos",MODE_PRIVATE);

        //alguien esta conectado
        String emailConectado = sharedPreferences.getString("email","");


        if (!emailConectado.equals("")){
            //SI EMAIL NO ESTA VACIO SIGNIFICA QUE SI HAY ALGUIEN CONECTADO
            abrirMenu(); //llamando al metodo para el meno
        }else{
            //En el caso que nadie este conectado

        }



    }

    //DEFINIR METODO
    public void abrirMenu(){
        Intent ventana = new Intent(getApplicationContext(), MainActivity2.class);
        startActivity(ventana);
        finish();
    }

    public void validarDatos(View v){
        String emailIngresado=email.getText().toString();
        String password = pass.getText().toString();
        //almacenando el email dentro de sharedPreferences

        if(emailIngresado.equals("edison@gmail.com") && password.equals("1234")){
            SharedPreferences contenido=getSharedPreferences("datos", MODE_PRIVATE);
            SharedPreferences.Editor editor1 =contenido.edit();
            editor1.putString("email",emailIngresado);//guardar el email del usuario conectado
            editor1.commit();//Confirmando los cambios realizados
            abrirMenu();
        }else{
            Toast.makeText(getApplicationContext(),"Email o password incorrectos",Toast.LENGTH_LONG).show();
        }
    }

    public void salir(View v){
        finish();
    }


}