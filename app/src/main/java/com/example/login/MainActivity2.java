package com.example.login;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity2 extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Derfinir objeto sharedPreferenced
        sharedPreferences = getSharedPreferences("datos",MODE_PRIVATE);//redireccionar al archivo creado con los datos

    }

    //Construccion de pagina principal
    public void login(){
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        finish();
    }

    //Contruccion del boton de validacion
    public void ValidarInicio(View v){

        SharedPreferences editar = getSharedPreferences("datos",Context.MODE_PRIVATE);//Extraccion de datos del XML
        SharedPreferences.Editor editor = editar.edit();
        editor.clear();//Limpiar datos para salir
        editor.apply();//Aplicar cambios el archivo
        finish();//Finalizar proceso
        login();//Redireccionar a la pagina de inicio

    }



}